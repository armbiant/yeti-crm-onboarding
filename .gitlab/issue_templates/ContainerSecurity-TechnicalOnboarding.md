### Overview

**This is the Onboarding Issue for Container Security**.

You should also have been assigned to a [Generic Onboarding Issue#overview](https://gitlab.com/gitlab-org/threat-management/onboarding/-/blob/master/.gitlab/issue_templates/Onboarding.md). If you need any help, please refer to the Slack channels listed in the generic issue.


In general, the features for the [Container Security (CS) categories](https://about.gitlab.com/handbook/product/product-categories/#container-security-group) are strongly related to [environments](https://docs.gitlab.com/ee/ci/environments/) and [Cluster Integration](https://docs.gitlab.com/ee/user/project/clusters/).

This document will help you setup your local development environment so you can run and develop these features locally, on your own machine.

#### Don't get too hung up if some of these tasks fail you

This Onboarding Issue is supposed to get you familiar with a number of GitLab features tangential to Container Scanning.
Its purpose is not to provide a list of necessary steps to reach a productive development environment.
This means that if some of the steps outlined here don't work for you, this will not block you from contributing in the future.
If you run into non-fundamental problems, do not invest large amounts of time into their resolution.
Instead, document what does not work for you, @mention your Onboarding Buddy, and continue with your other onboarding tasks.

#### General

1. [ ] Browse the [Protect](https://gitlab.com/gitlab-org/protect) namespace to discover sub-groups for mentions (e.g.: `@gitlab-org/protect/container-security-frontend`, `@gitlab-org/protect/container-security-backend`, `@gitlab-org/protect/managers`) and projects that can be helpful but which do not directly affect the product, such as examples and demos.

### Day 6: Setup

<details>
<summary>Click to expand/contract</summary>

#### Kubectl and GCP

Our Defend features are tightly integrated into GitLab's [Cluster Integration feature](https://docs.gitlab.com/ee/user/project/clusters/) and often require us to work with clusters directly. As we are a multicloud application we support various cloud providers but primarily GCP. As such, this configuration is focused around GCP [although more AWS Integrations are coming](https://gitlab.com/groups/gitlab-org/-/epics/1328).

1. [ ] [Install Google Cloud SDK](https://cloud.google.com/sdk/docs/downloads-interactive)
1. [ ] Authenticate using your GCP credentials
1. [ ] Install `kubectl`: `gcloud components install kubectl`
1. [ ] [Install `helm` v3](https://helm.sh/docs/intro/install/)

</details>

#### Minikube and local registry on macOS

<details>
<summary>Click to expand/contract</summary>

##### Pre-requisite
This short guide was written and tested with the following configuration:

- macOS Catalina: 10.15.4 (19E287)
- Docker engine: 19.03.8
- Minikube: v1.9.2
- Kubectl: v1.15.11

##### Install dependencies

- [ ] [Rancher Desktop](https://rancherdesktop.io/) (or [alternatives](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop))
- [ ] [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [ ] [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-macos).
- [ ] [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit#installation)
- [ ] [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md#docker-configuration)
- [ ] [Enable local
  registry](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md).
  Skip `docker-machine` steps.
- [ ] Add the registry IP address and port that you used above to the insecure registries in the
  Docker configuration.

#### Docker TLS

Your local Docker registry requires self-signed certificates with a SAN matching your local
development hostname (e.g. gdk.test). Add the following to your `gdk.yml`:

```
registry:
  host: gdk.test
```

For more info view: https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/1619

If you are running a Linux host you may want to configure the Docker daemon
with the following settings by modifying `/etc/docker/daemon.json` and restarting
the docker daemon (`sudo systemctl restart docker`).

```json
{
  "insecure-registries": ["gdk.test:5000", "gitlab.test:5000"]
}
```

If you have a CI job which uses the Docker daemon, you can also allow it to talk to insecure registries by passing flags to dind.

```yaml
include:
- template: Jobs/Build.gitlab-ci.yml

build:
  services:
    - name: 'docker:20.10.6-dind'
      command: ['--tls=false', '--host=tcp://0.0.0.0:2375', '--insecure-registry=gdk.test:5000']
```

##### GDK configuration

- [ ] Make sure to have enabled the local registry while specifying the hostname of your instance as the following:

```
registry:
  enabled: true
  host: <YOUR_IP>
  api_host: <YOUR_IP>
hostname: <YOUR_IP>
```

##### Gitlab Runner in privileged mode

- [ ] Make sure the configuration file has privileged set to true and that you share your local
  docker daemon file with gitlab runner. I have used `docker:stable` as image and runner settings
  for cache to be secure. It should look something similiar to this:

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "main-runner"
  url = "<YOUR_IP>:<YOUR_PORT>"
  token = "<YOUR_TOKEN>"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]
    shm_size = 0
  [runners.cache]
    Insecure = false
    [runners.cache.s3]
    [runners.cache.gcs]
```

Depending on how you have configured the default Docker network you may run into
instances where you might see the following error message:

```bash
 ---> Running in eb7910e691b6
 npm ERR! code EAI_AGAIN
 npm ERR! errno EAI_AGAIN
 npm ERR! request to https://registry.npmjs.org/cookie-parser failed, reason: getaddrinfo EAI_AGAIN registry.npmjs.org:443
 npm ERR! A complete log of this run can be found in:
 npm ERR!     /root/.npm/_logs/2020-10-21T19_02_20_647Z-debug.log
 The command '/bin/sh -c npm install' returned a non-zero code: 1
 ERROR: Job failed: exit code 1
```

If this occurs you may need to specify a different network mode in the `gitlab-runner` configuration file.

E.g.

```yaml
  [runners.docker]
    network_mode = "host"
```

Note: `gdk restart` and `gdk reconfigure` are required when changing `gitlab.yml` file. The same goes for `gitlab-runner restart` and `config.toml`. Sometimes when trying different configurations it is easy to miss.

##### Local cluster setup

- [ ] Start a new cluster. Replace `<REGISTRY_IP>` and `<REGISTRY_PORT>` with the information from the [Install dependencies](#install-dependencies) local registry step:

```
minikube start --memory=8192 --cpus=4 --insecure-registry="<REGISTRY_IP>:<REGISTRY_PORT>"
```
Note: Kubernetes v1.16.x [deprecated](https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/) some required extensions.

You can lauch the minikube dashboard to explore the different settings by running the following command from a terminal:

```bash
$ minikube dashboard
```
- [ ] In your local instance of gitlab, go to Admin->Settings->Network (http://gdk.test:3000/admin/application_settings/network) and make sure those options are checked:
```
Allow requests to the local network from web hooks and services
Allow requests to the local network from system hooks 
```
- [ ] Now you are ready to follow the same steps as described in the [adding an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) guide.
- [ ] Create and configure [Cluster Management Project](https://docs.gitlab.com/ee/user/clusters/management_project.html) to be able to manage automatic installation of new application to you cluster.
- [ ] Install [Ingress](https://docs.gitlab.com/ee/user/infrastructure/clusters/manage/management_project_applications/ingress.html), create a tunnel so that any load balancer with pending external ips can be provided with one. This can achieved with:

```
minikube tunnel
```

- [ ] Install [Prometheus](https://docs.gitlab.com/ee/user/infrastructure/clusters/manage/management_project_applications/prometheus.html), enable Prometheus integration in your cluster settings, and refresh the page, the metrics should be available in the UI (Health tab in your cluster configuration) and your environment should be ready to go.

Cluster API URL & IP can be found with the following commands:
```
$ kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
https://192.168.64.8:8443
$ minikube ip
192.168.64.8
```
</details>

### Day 7: Protect playground

#### Familiarize yourself with product features 

<details>
<summary>Click to expand/contract</summary>

##### Container Security

Review documentation related to Container Security to begin to understand the features you will be supporting.

1. [ ] [Security Policies](https://docs.gitlab.com/ee/user/application_security/policies)
1. [ ] [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)


#### Auto DevOps (on gitlab.com)

<details>
<summary>Click to expand/contract</summary>

GitLab can automatically configure the entire CI/CD pipeline for projects (without the need to create the `.gitlab-ci.yml` file), this feature is known as [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)

Auto DevOps may seem complex but uses the same underlying CI configuration as our customers. You can easily examine [the ADO template that we ship with each version of GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml).

Auto DevOps will automatically build, test, and deploy your application if all prerequisites are satisfied. We will start with simply building and testing your application on gitlab.com.

As part of this task you need to:
1. [ ] [Request the Ultimate tier](https://about.gitlab.com/handbook/incentives/#gitlab-ultimate) for your gitlab.com account to avoid being prompted to validate your credit card.
1. [ ] Create a new Express.js project on GitLab.com
1. [ ] Create Kubernetes cluster for your project by following this [guide](https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html), your GitLab google account should already have access to the `group-defend` [GCP](https://cloud.google.com) project; create a cluster in the `group-defend` project. Consider using preemtible VMs (when creating new cluster, go to Node Pools -> default-pool -> Nodes -> Enable preemptible nodes) and decrease number of nodes used in your cluster.
1. [ ] Install the required [GitLab Managed Apps](https://docs.gitlab.com/ee/user/clusters/applications.html) as mentioned in the documentation linked above: Helm, Ingress, and Runner.
1. [ ] In your project go to ` Settings > CI/CD > Auto DevOps` and enable it. This should automatically spawn a new pipeline. When prompted for a domain name, consider using a DNS service like [nip.io](https://nip.io), [sslip.io](https://sslip.io), etc.
1. [ ] Make sure your pipeline (`CI/CD > Pipelines`) has an `Auto DevOps` label on it and that it eventually passes.
1. [ ] Visit the `Operations > Environments` page and "Open live environment" to view your newly deployed application.
1. [ ] Visit `Operations > Pod Logs` to view logs from your application
1. [ ] If you have installed Prometheus, visit `Operations > Metrics` to view metrics for your cluster and application.
1. [ ] If you are not going to work on Auto DevOps remember to remove your cluster from [GCP](https://cloud.google.com).

</details>

#### Interacting directly with cluster

<details>
<summary>Click to expand/contract</summary>

We have now deployed an application and gitlab managed applications using GitLab's UI. Occasionally we must debug or access the cluster directly to tail logs, check events, create/destroy resources. To do this we can connect directly to the cluster using kubectl and helm.

1. [ ] Pull credentials for the newly created cluster into your kubectl context: `gcloud container clusters get-credentials <my_cluster_name> --zone <my_zone> --project group-defend-c8e44e`
1. [ ] Verify `kubectl` is properly connected with `kubectl get pods --all-namespaces`
1. [ ] (Optionally) setting up `helm`

</details>

#### GitLab Agent Server

1. [ ] [GitLab Docs](https://docs.gitlab.com/ee/user/clusters/agent/)
1. [ ] [Walkthrough Video](https://www.youtube.com/watch?v=PcVsnF9xCZI)
1. [ ] [GDK Docs](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md)

</details>

### Now you are ready for new tasks
You're awesome!
