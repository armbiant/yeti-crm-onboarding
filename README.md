# Onboarding - Threat Management

This project helps the [Threat Management](https://about.gitlab.com/handbook/engineering/development/threat-management/) sub-department to onboard its new members.

An issue template is used to create onboarding issues, which follows the general onboarding ones. That's why they start at day 6.

There are individual templates for the following groups:
 - [Container Security](.gitlab/issue_templates/CS_Technical_Onboarding.md)
 - [Vulnerability Management](.gitlab/issue_templates/Technical_Onboarding.md)
